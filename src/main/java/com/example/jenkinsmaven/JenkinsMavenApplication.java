package com.example.jenkinsmaven;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;
import reactor.core.publisher.Mono;

@SpringBootApplication
public class JenkinsMavenApplication {

    public static void main(String[] args) {
        SpringApplication.run(JenkinsMavenApplication.class, args);
    }

}

@RestController
class Controller {
    @GetMapping
    public Mono<String> hello() {
        return Mono.just("Hello");
    }
}
