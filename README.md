# Jenkins CI/CD release process

Testing out maven release CI/CD on Jenkins

---

## CI/CD

---

### Setting up Jenkins pipeline

1. Start local Jenkins server
   1.1 Pull Jenkins Docker image:
  ```bash
  docker pull jenkins/jenkins:lts
  ```
1.2 Run jenkins local server with the following command:
  ```bash
  docker run --name jenkins-server -p 8080:8080 -p 50000:50000 -v jenkins_home:/var/jenkins_home -d jenkins/jenkins:lts
  ```
1.3 Fetch the admin password to login to Jenkins
  ```bash
  docker logs jenkins-server
  ```
2. Login to Jenkins server through http://localhost:8080
   2.1 Install all recommended plugins
   2.2 Register your admin profile
   2.3 Complete the flow and you are done!
3. Install NodeJS plugin
    - Optional setup
        - If you want a **better looking UI** download **Blue Ocean** plugin
        - If you want **Docker support** download **Docker** plugin
4. Ensure that you have enabled the **Workspace Cleanup** plugin
5. Go to `Dashboard -> Manage Jenkins -> Global Tool Configuration` and a NodeJS installation. Here you
   can also install global packages to avoid adding them in your build script.
6. From here you can create your multi-branch pipeline and test things out.

---

### Adding Jenkins cron job

Since we are accumulating node caches, we will need to periodically delete them when they pass certain size.
To create Jenkins cron job:
1. Create new freestyle project
2. Set in build triggers: Build periodically
3. Enter schedule, for example every day: `0 0 * * *`
4. Add build step `Execute shell`
5. Add shell script from `scripts/cache-cleanup.sh`
6. Hit save and you are done. If you are using slaves, be sure to select on which slave it should run.

---

## Troubleshooting
- Issues when running Puppeteer on CI like Jenkins
    - https://github.com/puppeteer/puppeteer/blob/main/docs/troubleshooting.md
- Missing `ll` alias? Create it with `alias ll='ls -alF'`
- Check directory size in Linux: `sudo du -sh ./my-directory`

---

## Further help

To get more help on the Angular CLI use `ng help` or go check out the [Angular CLI README](https://github.com/angular/angular-cli/blob/master/README.md).

